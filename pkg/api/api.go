package api

import (
	"context"
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	log "github.com/sirupsen/logrus"
	"net/http"
	"reflect"
)

func OK(ctx context.Context, vv ...interface{}) (events.APIGatewayV2HTTPResponse, error) {
	return response(ctx, http.StatusOK, vv)
}

func BadRequest(ctx context.Context, err error) (events.APIGatewayV2HTTPResponse, error) {
	return response(ctx, http.StatusBadRequest, err)
}

func NotImplemented(ctx context.Context) (events.APIGatewayV2HTTPResponse, error) {
	return response(ctx, http.StatusNotImplemented)
}

func response(ctx context.Context, code int, vv ...interface{}) (events.APIGatewayV2HTTPResponse, error) {

	res := events.APIGatewayV2HTTPResponse{
		Headers:         map[string]string{"Content-Type": "application/json"},
		IsBase64Encoded: false,
		StatusCode:      code,
	}

	if vv == nil || len(vv) == 0 {
		log.WithContext(ctx).
			WithField("code", code).
			Info("response returned")
		return res, nil
	}

	if v := vv[0]; reflect.ValueOf(v).Type().Implements(reflect.TypeOf((*error)(nil)).Elem()) {
		res.Body = v.(error).Error()
	} else if data, err := json.Marshal(&v); err != nil {
		log.WithError(err).Error("while marshalling JSON for API Response!")
		res.StatusCode = http.StatusInternalServerError
		res.Body = err.Error()
	} else {
		res.Body = string(data)
	}

	log.WithContext(ctx).
		WithField("code", res.StatusCode).
		WithField("body", res.Body).
		Info("response returned")

	return res, nil
}
