package db

import (
	"context"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	log "github.com/sirupsen/logrus"
)

type Entity interface {
	PutInput() *dynamodb.PutItemInput
}

var client *dynamodb.Client

func init() {
	if cfg, err := config.LoadDefaultConfig(context.Background()); err != nil {
		log.WithError(err).Fatal()
	} else {
		client = dynamodb.NewFromConfig(cfg)
	}
}

func Put(ctx context.Context, e Entity) (err error) {
	var out *dynamodb.PutItemOutput
	if out, err = client.PutItem(ctx, e.PutInput()); err != nil {
		log.WithError(err).Error("while putting entity")
	} else if err = attributevalue.UnmarshalMap(out.Attributes, e); err != nil {
		log.WithError(err).Error("while unmarshalling map from put output")
	}
	return
}
