package model

import (
	"AD-25/pkg/api"
	"AD-25/pkg/db"
	"context"
	"encoding/json"
	"errors"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	log "github.com/sirupsen/logrus"
	"os"
	"time"
)

type Feedback struct {
	Stamp      string `json:"stamp" dynamodbav:"stamp"`
	Unix       int64  `json:"unix" dynamodbav:"unix"`
	Rating     int    `json:"rating" dynamodbav:"rating"`
	Comments   string `json:"comments" dynamodbav:"comments"`
	AppName    string `json:"appName" dynamodbav:"app_name"`
	AppVersion string `json:"appVersion" dynamodbav:"app_version"`
}

func (f *Feedback) PutInput() *dynamodb.PutItemInput {
	if f.Stamp == "" {
		now := time.Now().UTC()
		f.Stamp = now.Format("01/02/06")
		f.Unix = now.UnixNano()
	}
	item, _ := attributevalue.MarshalMap(f)
	return &dynamodb.PutItemInput{
		Item:      item,
		TableName: tableName,
	}
}

var tableName *string

func init() {
	if table := os.Getenv("TABLE_NAME"); table == "" {
		log.WithError(errors.New("TABLE_NAME env var is empty")).Fatal()
	} else {
		tableName = &table
	}
}

func Save(ctx context.Context, req events.APIGatewayV2HTTPRequest) (events.APIGatewayV2HTTPResponse, error) {
	var e Feedback
	if err := json.Unmarshal([]byte(req.Body), &e); err != nil {
		log.WithError(err).Error("while marshalling JSON from API Request!")
		return api.BadRequest(ctx, err)
	} else if err = db.Put(ctx, &e); err != nil {
		return api.BadRequest(ctx, err)
	}
	return api.OK(ctx, &e)
}
