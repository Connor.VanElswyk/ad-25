package main

import (
	"AD-25/pkg/model"
	"context"
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"net/http"
	"testing"
)

func Test_OK(t *testing.T) {

	data, _ := json.Marshal(model.Feedback{
		Rating:     5,
		Comments:   "Great job!",
		AppName:    "CHADVASC",
		AppVersion: "1.0.0",
	})

	req := events.APIGatewayV2HTTPRequest{
		RequestContext: events.APIGatewayV2HTTPRequestContext{
			HTTP: events.APIGatewayV2HTTPRequestContextHTTPDescription{
				Method: http.MethodPut,
			},
		},
		Body: string(data),
	}

	execute(t, req, http.StatusOK)

	req.RequestContext.HTTP.Method = http.MethodHead
	execute(t, req, http.StatusOK)

	req.RequestContext.HTTP.Method = http.MethodConnect
	execute(t, req, http.StatusOK)

	req.RequestContext.HTTP.Method = http.MethodOptions
	execute(t, req, http.StatusOK)

	req.RequestContext.HTTP.Method = http.MethodTrace
	execute(t, req, http.StatusOK)
}

func Test_NotImplemented(t *testing.T) {

	req := events.APIGatewayV2HTTPRequest{
		RequestContext: events.APIGatewayV2HTTPRequestContext{
			HTTP: events.APIGatewayV2HTTPRequestContextHTTPDescription{
				Method: http.MethodGet,
			},
		},
	}
	execute(t, req, http.StatusNotImplemented)

	req.RequestContext.HTTP.Method = http.MethodDelete
	execute(t, req, http.StatusNotImplemented)

	req.RequestContext.HTTP.Method = http.MethodPatch
	execute(t, req, http.StatusNotImplemented)
}

func Test_BadRequest(t *testing.T) {
	req := events.APIGatewayV2HTTPRequest{
		RequestContext: events.APIGatewayV2HTTPRequestContext{
			HTTP: events.APIGatewayV2HTTPRequestContextHTTPDescription{
				Method: http.MethodPost,
			},
		},
		Body: `{foo:bar}`,
	}
	execute(t, req, http.StatusBadRequest)
}

func execute(t *testing.T, req events.APIGatewayV2HTTPRequest, status int) {
	if res, _ := Handler(context.TODO(), req); res.StatusCode != status {
		t.Error(res.StatusCode, res.Body)
	}
}
