package main

import (
	"AD-25/pkg/api"
	"AD-25/pkg/model"
	"context"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
)

func init() {

	log.SetOutput(os.Stdout)
	log.SetFormatter(&log.TextFormatter{
		EnvironmentOverrideColors: true,
		ForceColors:               true,
	})

	lvl, err := log.ParseLevel(os.Getenv("LOG_LEVEL"))
	if err != nil {
		log.WithError(err).Warn("while getting env var LOG_LEVEL")
		lvl = log.DebugLevel
	}
	log.SetLevel(lvl)
}

func Handler(ctx context.Context, req events.APIGatewayV2HTTPRequest) (events.APIGatewayV2HTTPResponse, error) {

	log.WithContext(ctx).
		WithField("method", req.RequestContext.HTTP.Method).
		WithField("body", req.Body).
		Info("request received")

	switch req.RequestContext.HTTP.Method {

	case http.MethodHead:
		fallthrough
	case http.MethodConnect:
		fallthrough
	case http.MethodOptions:
		fallthrough
	case http.MethodTrace:
		return api.OK(ctx)
	case http.MethodPost:
		fallthrough
	case http.MethodPut:
		return model.Save(ctx, req)
	case http.MethodPatch:
		fallthrough
	case http.MethodDelete:
		fallthrough
	case http.MethodGet:
		fallthrough
	default:
		return api.NotImplemented(ctx)
	}
}

func main() {
	lambda.Start(Handler)
}
